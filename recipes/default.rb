#
# Cookbook:: sublime_text
# Recipe:: default
#
# Copyright:: 2017, The Authors, All Rights Reserved.

if platform?('centos') or platform?('ubuntu')
  include_recipe 'sublime_text::debian' if platform_family?('debian')
  
  include_recipe 'sublime_text::packages'
  include_recipe 'sublime_text::syntax-specific'
  
  node['sublime_text']['users'].each do |user|
    template "/home/#{user}/.config/sublime-text-3/Packages/User/Preferences.sublime-settings" do
      cookbook 'sublime_text'
      source 'Preferences.sublime-settings.erb'
      owner user
      group user
      mode '0644'
      variables(
        preferences: node['sublime_text']['preferences']
      )
    end
  end
end

include_recipe 'sublime_text::windows' if node['platform'] == 'windows'
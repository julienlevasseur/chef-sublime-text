#
# Cookbook:: chef-sublime-text
# Recipe:: windows
#
# Copyright:: 2018, Julien Levasseur, All Rights Reserved.

chocolatey_package 'sublimetext3' do
  action :install
end

execute 'install_sublime_text_3_package_control' do
  command 'echo \'st3\'|choco install -y sublimetext3.packagecontrol'
  creates '/tmp/something'
  action :run
end

node['sublime_text']['users'].each do |user|
  directory "C:\\Users\\#{user}\\AppData\\Roaming\\Sublime Text 3" do
    recursive true
    rights :full_control, user
    inherits false
    action :create
  end

  directory "C:\\Users\\#{user}\\AppData\\Roaming\\Sublime Text 3\\Packages\\User" do
    recursive true
    rights :full_control, user
    inherits false
    action :create
  end

  directory "C:\\Users\\#{user}\\AppData\\Roaming\\Sublime Text 3\\Installed Packages" do
    recursive true
    rights :full_control, user
    inherits false
    action :create
  end

  template "C:\\Users\\#{user}\\AppData\\Roaming\\Sublime Text 3\\Packages\\User\\Package Control.sublime-settings" do
    cookbook 'sublime_text'
    source 'Package_Control.sublime-settings.erb'
    owner user
    group user
    mode '0644'
    variables(
      packages: node['sublime_text']['packages']
    )
  end

  node['sublime_text']['packages'].each do |pkg|
    remote_file "C:\\Users\\#{user}\\AppData\\Roaming\\Sublime Text 3\\Installed Packages/#{pkg['name']}.sublime-package" do
        source pkg['url']
        owner user
        group user
        mode '0644'
        action :create
      end
  end

  node['sublime_text']['syntax_specific'].each do |language, options|
    template "C:\\Users\\#{user}\\AppData\\Roaming\\Sublime Text 3\\Packages\\User\\#{language}.sublime-settings" do
      cookbook 'sublime_text'
      source 'language.sublime-settings.erb'
      owner user
      group user
      mode '0644'
      variables(
        options: options
      )
    end
  end

  cookbook_file "C:\\Users\\#{user}\\AppData\\Roaming\\Sublime Text 3\\Packages\\User\\Terraform.sublime-settings" do
    source 'Terraform.sublime-settings'
    owner user
    group user
    mode '0644'
  end

end
